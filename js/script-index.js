
$(document).ready(function () {

    $(".step-one-content, .step-two-content, .step-three-content").hide();

    let flagOne = false;
    let flagTwo = false;
    let flagThree = false;

    $(".step-one").on("click", function () {
        $(".step-one-content").toggle("slow");
        rotateArrowOne();
        if (flagTwo) {
            rotateArrowTwo();
            $(".step-two-content").hide("slow");
        }
        if (flagThree) {
            rotateArrowThree();
            $(".step-three-content").hide("slow");
        }
    });

    $(".step-two").on("click", function () {
        $(".step-two-content").toggle("slow");
        rotateArrowTwo();
        if (flagOne) {
            rotateArrowOne();
            $(".step-one-content").hide("slow");
        }
        if (flagThree) {
            rotateArrowThree();
            $(".step-three-content").hide("slow");
        }
    });

    $(".step-three").on("click", function () {
        $(".step-three-content").toggle("slow");
        rotateArrowThree();
        if (flagOne) {
            rotateArrowOne();
            $(".step-one-content").hide("slow");
        }
        if (flagTwo) {
            rotateArrowTwo();
            $(".step-two-content").hide("slow");
        }
    });

    function rotateArrowOne() {
        if (!flagOne) {
            $("#caret-step-one").css({
                "transform": "rotate(90deg)",
                "-webkit-transition": "transform 1s ease-in-out"
            });
        } else {
            $("#caret-step-one").css({
                "transform": "rotate(0deg)"
            });
        }
        flagOne = !flagOne;
    }

    function rotateArrowTwo() {

        if (!flagTwo) {
            $("#caret-step-two").css({
                "transform": "rotate(90deg)",
                "-webkit-transition": "transform 1s ease-in-out"
            });
        } else {
            $("#caret-step-two").css({
                "transform": "rotate(0deg)"
            });
        }
        flagTwo = !flagTwo;
    }

    function rotateArrowThree() {

        if (!flagThree) {
            $("#caret-step-three").css({
                "transform": "rotate(90deg)",
                "-webkit-transition": "transform 1s ease-in-out"
            });
        } else {
            $("#caret-step-three").css({
                "transform": "rotate(0deg)"
            });
        }
        flagThree = !flagThree;
    }


});

var planSize = 0;

if (document.getElementById("beginner")) {
    document.getElementById("beginner").addEventListener("click", function () {
        sessionStorage.planSize = this.value;
        window.location.href = "./Home.html"; 
    });
}

if (document.getElementById("intermediate")) {
    document.getElementById("intermediate").addEventListener("click", function () {
        sessionStorage.planSize = this.value;
        window.location.href = "./Home.html"; 
    })
}

if (document.getElementById("advanced")) {
    document.getElementById("advanced").addEventListener("click", function () {
        sessionStorage.planSize = this.value;
        window.location.href = "./Home.html";
    })
}