// let planSize = parseInt(window.prompt("Enter an numnber : "));

planSize = parseInt(sessionStorage.planSize);

let timer = document.getElementById("timer");
let secound = 0;
let minute = 0;
let isPause = false;
let myInterval = setInterval(function () {
    if (!isPause) {
        timer.innerHTML = ("0" + minute).slice(-2) + " : " + ("0" + secound).slice(-2);
        secound++;
        if (secound === 60) {
            minute++;
            secound = 0;
        }
    }
}, 1000);

document.getElementById("pause").addEventListener("click", function () {
    if (!isPause) {
        this.classList.remove("fa-pause-circle");
        this.classList.add("fa-play-circle");
        this.children[0].innerHTML = "Play";
        isPause = true;
        pauseMode(true);
    } else {
        this.classList.add("fa-pause-circle");
        this.classList.remove("fa-play-circle");
        this.children[0].innerHTML = "Pause";
        isPause = false;
        pauseMode(false);
    }
});

function pauseMode(boolean) {
    if (boolean === true) {
        document.getElementById("myPlan").style.display = "none";
        let newDiv = document.createElement("div");
        newDiv.style.display = "flex";
        newDiv.style.margin = "30px auto 30px auto";
        newDiv.style.width = "400px";
        newDiv.style.height = "300px";
        newDiv.style.backgroundColor = "rgba(128, 128, 128, 0.445)";
        newDiv.style.border = "1px solid gray";
        newDiv.style.borderRadius = "5px";
        newDiv.setAttribute("id", "pauseContainer");
        let pTag = document.createElement("p");
        pTag.style.margin = "auto";
        pTag.style.color = "white";
        pTag.style.fontSize = "46px";
        pTag.appendChild(document.createTextNode("PAUSE !"));
        newDiv.appendChild(pTag);
        document.getElementsByClassName("container")[0].appendChild(newDiv);
    } else {
        document.getElementById("myPlan").style.display = "block";
        document.getElementById("pauseContainer").remove();
    }
}

let bombNum = 0;
let flagNum = 0;
switch (planSize) {
    case 10:
        bombNum = 15;
        flagNum = 15;
        document.getElementsByClassName("game-controler")[0]
            .children[0].children[0].innerHTML = bombNum;
        document.getElementsByClassName("game-controler")[0]
            .children[1].children[0].innerHTML = flagNum;
        break;

    case 16:
        bombNum = 40;
        flagNum = 40;
        document.getElementsByClassName("game-controler")[0]
            .children[0].children[0].innerHTML = bombNum;
        document.getElementsByClassName("game-controler")[0]
            .children[1].children[0].innerHTML = flagNum;
        break;

    case 30:
        bombNum = 100;
        flagNum = 100;
        document.getElementsByClassName("game-controler")[0]
            .children[0].children[0].innerHTML = bombNum;
        document.getElementsByClassName("game-controler")[0]
            .children[1].children[0].innerHTML = flagNum;
        break;
}

let gamePlanContainer = document.getElementsByClassName("game-plan")[0];
let myPlan = document.getElementById("myPlan");

function planGenerator() {
    for (let i = 0; i < planSize; i++) {
        let row = myPlan.insertRow(i);
        for (let j = 0; j < planSize; j++) {
            let cell = row.insertCell(j);
            cell.setAttribute("mine", "false");
        }
    }
}

planGenerator();

let bomb = '<i class="fas fa-bomb set-bomb-size"></i>';
let arrayLocationBombsX = [];
let arrayLocationBombsY = [];

function insertRandomBombs() {
    for (let i = 0; i < bombNum; i++) {

        let randomRow = Math.floor((Math.random() * planSize));
        let randomCell = Math.floor((Math.random() * planSize));

        if (myPlan.rows[randomRow].cells[randomCell].getAttribute("mine") === "false") {
            myPlan.rows[randomRow].cells[randomCell].setAttribute("mine", "true");
            myPlan.rows[randomRow].cells[randomCell].innerHTML = bomb;
            myPlan.rows[randomRow].cells[randomCell].childNodes[0].style.display = "none";
            arrayLocationBombsX.push(randomRow);
            arrayLocationBombsY.push(randomCell);
        } else {
            i -= 1;
        }
    }
}

insertRandomBombs();

function cellAction(cellTag) {
    if (cellTag.getAttribute("mine") === "true") {
        gameOverMode();
    } else {

        let bombCounter = 0;

        let y = cellTag.cellIndex;
        let x = cellTag.parentNode.rowIndex;

        checker(x - 1, y - 1);
        checker(x, y - 1);
        checker(x + 1, y - 1);
        checker(x - 1, y);
        checker(x + 1, y);
        checker(x - 1, y + 1);
        checker(x, y + 1);
        checker(x + 1, y + 1);

        function checker(Xposition, Yposition) {
            if (myPlan.rows[Xposition] !== undefined) {
                if (myPlan.rows[Xposition].cells[Yposition] !== undefined) {
                    for (let i = 0; i < arrayLocationBombsY.length; i++) {
                        if ((arrayLocationBombsX[i] === Xposition) && (arrayLocationBombsY[i] === Yposition)) {
                            bombCounter = bombCounter + 1;
                        }
                    }
                }
            }
        }

        cellTag.style.backgroundColor = "whitesmoke";

        function styleNumbers(number, color) {
            if (bombCounter === number) {
                cellTag.style.color = color;
            }
        }

        styleNumbers(0, "whitesmoke");
        styleNumbers(1, "#16d298");
        styleNumbers(2, "#0096ef");
        styleNumbers(3, "#f0b92d");
        styleNumbers(4, "#f04d39");
        styleNumbers(5, "red");
        styleNumbers(6, "red");
        styleNumbers(7, "red");
        styleNumbers(8, "red");

        cellTag.innerHTML = "<p>" + bombCounter + "</p>";

        if (bombCounter === 0) {
            for (let i = x - 1; i <= x + 1; i++) {
                for (let j = y - 1; j <= y + 1; j++) {
                    if (myPlan.rows[i] !== undefined) {
                        if (myPlan.rows[i].cells[j] !== undefined) {
                            if (myPlan.rows[i].cells[j].innerHTML === "") {
                                myPlan.rows[i].cells[j].style.pointerEvents = "none";
                                cellAction(myPlan.rows[i].cells[j]);
                            }
                        }
                    }
                }
            }
        }
    }

    if (winMode()) {
        let newDiv = document.createElement("div");
        newDiv.style.display = "flex";
        newDiv.style.margin = "10px auto 10px auto";
        newDiv.style.width = "450px";
        newDiv.style.height = "50px";
        newDiv.style.backgroundColor = "rgba(128, 128, 128, 0.445)";
        newDiv.style.border = "1px solid gray";
        newDiv.style.borderRadius = "5px";
        let pTag = document.createElement("p");
        pTag.style.margin = "auto";
        pTag.style.color = "white";
        pTag.style.fontSize = "16px";
        pTag.appendChild(document.createTextNode("Congratulations, you won in " + minute + " minute(s) and " + secound + " second(s)."));
        newDiv.appendChild(pTag);
        document.getElementsByClassName("container")[0].appendChild(newDiv);
    }
}


function styleNumbers(number, color) {
    if (bombCounter === number) {
        cellTag.style.color = color;
    }
}

function disableClick() {
    for (let i = 0; i < planSize * planSize; i++) {
        document.getElementsByTagName("td")[i].style.pointerEvents = "none";
    }
    let newDiv = document.createElement("div");
    newDiv.style.display = "flex";
    newDiv.style.margin = "10px auto 10px auto";
    newDiv.style.width = "300px";
    newDiv.style.height = "50px";
    newDiv.style.backgroundColor = "rgba(128, 128, 128, 0.445)";
    newDiv.style.border = "1px solid gray";
    newDiv.style.borderRadius = "5px";
    let pTag = document.createElement("p");
    pTag.style.margin = "auto";
    pTag.style.color = "white";
    pTag.style.fontSize = "16px";
    pTag.appendChild(document.createTextNode("You lost, Back to home and play again!"));
    newDiv.appendChild(pTag);
    document.getElementsByClassName("container")[0].appendChild(newDiv);

    clearInterval(myInterval);
}

function showAllBombs() {
    for (let i = 0; i < arrayLocationBombsX.length; i++) {
        myPlan.rows[arrayLocationBombsX[i]].cells[arrayLocationBombsY[i]].childNodes[0].style.display = "block";
        myPlan.rows[arrayLocationBombsX[i]].cells[arrayLocationBombsY[i]].style.backgroundColor = "#f8f8f8";
        myPlan.rows[arrayLocationBombsX[i]].cells[arrayLocationBombsY[i]].style.pointerEvents = "none";
    }
}

function gameOverMode() {
    showAllBombs();
    disableClick();
}

let emptyCellCounter = 0;

function winMode() {
    for (let i = 0; i < planSize * planSize; i++) {
        if (document.getElementsByTagName("td")[i].innerHTML === "") {
            return false;
        }
    }
    clearInterval(myInterval);
    showAllBombs();
    return true;
}

let currentCell = document.getElementsByTagName("td");
for (let i = 0; i < planSize * planSize; i++) {
    currentCell[i].addEventListener("click", function () {
        cellAction(this);
        this.style.pointerEvents = "none";
    });
}

function flagCounter(action) {
    let flagNum = parseInt(document.getElementById("flagCounter").innerHTML);
    if (action === "add") {
        flagNum--;
        document.getElementById("flagCounter").innerHTML = flagNum;
    }
    if (action === "reduce") {
        flagNum++;
        document.getElementById("flagCounter").innerHTML = flagNum;
    }
}

window.oncontextmenu = function (e) {
    let flagNum = parseInt(document.getElementById("flagCounter").innerHTML);
    if (e.target.tagName === "TD") {
        if (flagNum > 0) {
            if (e.target.getAttribute("flag") === "true") {
                if (e.target.getAttribute("mine") === "false") {
                    e.target.children[0].remove();
                    this.flagCounter("reduce");
                } else {
                    e.target.children[1].remove();
                    this.flagCounter("reduce");
                }
                e.target.setAttribute("flag", "false");
            } else {
                let flagTag = this.document.createElement("i");
                flagTag.classList.add("fab", "fa-font-awesome-flag");
                e.target.appendChild(flagTag);
                e.target.setAttribute("flag", "true");
                this.flagCounter("add");
            }
        }
    }

    if (e.target.parentNode.tagName === "TD" && e.target.parentNode.getAttribute("flag") === "true") {
        e.target.parentNode.setAttribute("flag", "false");
        e.target.remove();
        this.flagCounter("reduce");
    }

    return false;
}

document.getElementById("home").addEventListener("click", function () {
    window.location.href = "./index.html";
});